<?php

declare(strict_types=1);

namespace App\Rules;

use App\Validation\TelephoneNumberValidator\TelephoneNumberValidator;
use Illuminate\Contracts\Validation\Rule;

class TelephoneNumber implements Rule, DirtinessIncrementer
{
    protected const DIRTINESS_VALUE = 10;

    private TelephoneNumberValidator $validator;

    public function __construct(TelephoneNumberValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $number = $value['number'] ?? null;
        $countryCode = $value['countryCode'] ?? null;
        return $this->validator->isValid($number, $countryCode);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Your Phone Number does not appear to be valid. Valid phone numbers ' .
            'should be in the following format: 01234 567890';
    }

    public function getDirtinessValue(): int
    {
        return self::DIRTINESS_VALUE;
    }
}

<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TitleOfPerson implements Rule, DirtinessIncrementer
{
    protected const DIRTINESS_VALUE = 5;

    protected const VALID_TITLES = [
        'Mr',
        'Ms',
        'Dr',
        'Count',
    ];

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (in_array($value, self::VALID_TITLES)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Your Title is not valid. Valid titles are ' .
            implode(', ', self::VALID_TITLES);
    }

    public function getDirtinessValue(): int
    {
        return self::DIRTINESS_VALUE;
    }
}

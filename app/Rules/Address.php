<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;

class Address implements ImplicitRule, DirtinessIncrementer
{
    protected const DIRTINESS_VALUE = 20;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $addressLine1 = trim($value['addressLine1']) ?? null;
        $addressLine2 = trim($value['addressLine2']) ?? null;
        $townCity = trim($value['townCity']) ?? null;
        $county = trim($value['county']) ?? null;
        $postCode = trim($value['postCode']) ?? null;

        if
        (strlen($addressLine1) > 0
            && strlen($townCity) > 0
            && strlen($county) > 0
            && strlen($postCode) > 0
        ) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Your Address does not appear to be valid';
    }

    public function getDirtinessValue(): int
    {
        return self::DIRTINESS_VALUE;
    }
}

<?php
// @codingStandardsIgnoreFile

declare(strict_types=1);

namespace App\Rules;

use Tests\TestCase;

final class TitleOfPersonTest extends TestCase
{
    private TitleOfPerson $rule;
    private const VALID_TITLES = [
        'Mr',
        'Ms',
        'Dr',
        'Count',
    ];

    public function setUp(): void
    {
        $this->rule = new TitleOfPerson();

        parent::setUp();
    }

    /** @test */
    public function it_validates_a_persons_family_name_is_present(): void
    {
        $rule = $this->rule;
        $invalidTitle = 'FooBar';

        foreach (self::VALID_TITLES as $title) {
            $this->assertTrue($rule->passes('', $title));
        }

        $this->assertFalse($rule->passes('', $invalidTitle));
    }

    /** @test */
    public function it_return_a_dirtiness_value(): void
    {
        $rule = $this->rule;

        $this->assertIsInt($rule->getDirtinessValue());
    }
}

<?php
// @codingStandardsIgnoreFile

declare(strict_types=1);

namespace App\Rules;

use Tests\TestCase;

final class FamilyNameOfPersonTest extends TestCase
{
    private FamilyNameOfPerson $rule;

    public function setUp(): void
    {
        $this->rule = new FamilyNameOfPerson();

        parent::setUp();
    }

    /** @test */
    public function it_validates_a_persons_family_name_is_present(): void
    {
        $rule = $this->rule;

        $validFamilyName = 'FooBar';
        $emptyFamilyName = '';
        $nullFamilyName = null;

        $this->assertTrue($rule->passes('', $validFamilyName));
        $this->assertFalse($rule->passes('', $emptyFamilyName));
        $this->assertFalse($rule->passes('', $nullFamilyName));
    }

    /** @test */
    public function it_return_a_dirtiness_value(): void
    {
        $rule = $this->rule;

        $this->assertIsInt($rule->getDirtinessValue());
    }
}

<?php
// @codingStandardsIgnoreFile

declare(strict_types=1);

namespace App\Rules;

use Tests\TestCase;

final class AddressTest extends TestCase
{
    private Address $rule;

    public function setUp(): void
    {
        $this->rule = new Address();

        parent::setUp();
    }

    /** @test */
    public function it_validates_a_persons_address(): void
    {
        $rule = $this->rule;
        $validAddress = [
            'addressLine1' => 'Address Line 1',
            'addressLine2' => '',
            'townCity' => 'Town',
            'county' => 'County',
            'postCode' => 'AB12 3CD',
            'country' => 'United Kingdom',
        ];
        $invalidAddress = [
            'addressLine1' => '',
            'addressLine2' => 'Address Line 2',
            'townCity' => '',
            'county' => 'County',
            'postCode' => 'AB12 3CD',
            'country' => 'United Kingdom',
        ];

        $this->assertTrue($rule->passes('', $validAddress));
        $this->assertFalse($rule->passes('', $invalidAddress));
    }

    /** @test */
    public function it_return_a_dirtiness_value(): void
    {
        $rule = $this->rule;

        $this->assertIsInt($rule->getDirtinessValue());
    }
}

<?php
// @codingStandardsIgnoreFile

declare(strict_types=1);

namespace App\Rules;

use App\Validation\TelephoneNumberValidator\TelephoneNumberValidator;
use Prophecy\Argument;
use Tests\TestCase;

final class TelephoneNumberTest extends TestCase
{
    private $mockTelephoneNumberValidator;

    public function setUp(): void
    {
        $this->mockTelephoneNumberValidator = $this->prophesize(TelephoneNumberValidator::class);

        parent::setUp();
    }

    /** @test */
    public function it_validates_a_persons_telephone_number_is_valid(): void
    {
        $this->mockTelephoneNumberValidator->isValid(Argument::cetera())->willReturn(true);
        $rule = new TelephoneNumber($this->mockTelephoneNumberValidator->reveal());

        $this->assertTrue($rule->passes('', 'validPhoneNumber'));

        $this->mockTelephoneNumberValidator->isValid(Argument::cetera())->willReturn(false);
        $rule = new TelephoneNumber($this->mockTelephoneNumberValidator->reveal());

        $this->assertFalse($rule->passes('', 'invalidPhoneNumber'));
    }

    /** @test */
    public function it_return_a_dirtiness_value(): void
    {
        $rule = new TelephoneNumber($this->mockTelephoneNumberValidator->reveal());

        $this->assertIsInt($rule->getDirtinessValue());
    }
}

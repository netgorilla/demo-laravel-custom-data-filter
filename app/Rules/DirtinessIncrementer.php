<?php

declare(strict_types=1);

namespace App\Rules;

interface DirtinessIncrementer
{
    public function getDirtinessValue(): int;
}

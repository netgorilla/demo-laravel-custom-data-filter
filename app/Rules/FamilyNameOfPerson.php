<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;

class FamilyNameOfPerson implements ImplicitRule, DirtinessIncrementer
{
    protected const DIRTINESS_VALUE = 5;

    public function passes($attribute, $value): bool
    {
        if (strlen(trim($value ?? '')) > 0) {
            return true;
        }

        return false;
    }

    public function message(): string
    {
        return 'Your Surname is required';
    }

    public function getDirtinessValue(): int
    {
        return self::DIRTINESS_VALUE;
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DataParser\DataParser;
use Illuminate\Support\Collection;

class DataImporterController extends Controller
{
    private DataParser $customerDataParser;

    public function __construct(DataParser $customerDataParser)
    {
        $this->customerDataParser = $customerDataParser;
    }

    public function index()
    {
        $data = $this->getMockData();

        $parsedData = $this->customerDataParser->parse($data);

        return view('index', compact('data', 'parsedData'));
    }

    private function getMockData(): Collection
    {
        return new Collection(
            [
                [
                    'id' => 'fed8f237-dc07-46a2-8651-34dc153f6d3f',
                    'name' => [
                        'title' => 'Mr',
                        'givenName' => '',
                        'familyName' => '',
                    ],
                    'address' => [
                        'addressLine1' => '',
                        'addressLine2' => '',
                        'townCity' => 'Town',
                        'county' => 'County',
                        'postCode' => 'AB12 3CD',
                        'country' => 'United Kingdom',
                    ],
                    'phoneNumber' => [
                        'label' => 'mobile',
                        'number' => '123',
                        'countryCode' => 'GB',
                    ],
                ],
                [
                    'id' => '1dcf531a-8be3-4d6e-85f3-3a08c28c29d6',
                    'name' => [
                        'title' => 'Rev',
                        'givenName' => 'Colin',
                        'familyName' => 'Cook',
                    ],
                    'address' => [
                        'addressLine1' => 'Address Line 1',
                        'addressLine2' => '',
                        'townCity' => 'Town',
                        'county' => 'County',
                        'postCode' => 'CD34 5EF',
                        'country' => 'United Kingdom',
                    ],
                    'phoneNumber' => [
                        'label' => 'home',
                        'number' => '0123456789',
                        'countryCode' => '',
                    ],
                ],
            ]
        );
    }
}

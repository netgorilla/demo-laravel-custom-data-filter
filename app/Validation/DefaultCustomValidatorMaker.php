<?php

declare(strict_types=1);

namespace App\Validation;

use Illuminate\Contracts\Validation\Factory as ValidatorFactory;

final class DefaultCustomValidatorMaker implements CustomValidatorMaker
{
    private array $rules;
    private array $messages;
    private ValidatorFactory $validator;

    public function __construct(array $rules, array $messages, ValidatorFactory $validator)
    {
        $this->rules = $rules;
        $this->messages = $messages;
        $this->validator = $validator;
    }

    public function make(array $customerData): CustomValidator
    {
        return $this->validator->make($customerData, $this->rules, $this->messages);
    }
}

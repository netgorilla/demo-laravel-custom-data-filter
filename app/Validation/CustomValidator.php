<?php

declare(strict_types=1);

namespace App\Validation;

use Illuminate\Contracts\Validation\Validator;

interface CustomValidator extends Validator
{
    public function getTotalDirtinessScore(): int;

    public function incrementDirtiness(int $amount): void;
}

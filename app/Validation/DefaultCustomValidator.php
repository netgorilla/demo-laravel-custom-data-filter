<?php

declare(strict_types=1);

namespace App\Validation;

use App\Rules\DirtinessIncrementer;
use Illuminate\Validation\Validator;

final class DefaultCustomValidator extends Validator implements CustomValidator
{
    protected int $totalDirtinessScore = 0;

    public function incrementDirtiness(int $amount): void
    {
        $this->totalDirtinessScore += $amount;
    }

    public function getTotalDirtinessScore(): int
    {
        return $this->totalDirtinessScore;
    }

    protected function validateUsingCustomRule($attribute, $value, $rule)
    {
        if (! $rule->passes($attribute, $value)) {

            if ($rule instanceof DirtinessIncrementer) {
                $this->incrementDirtiness($rule->getDirtinessValue());
            }

            $this->failedRules[$attribute][get_class($rule)] = [];

            $messages = $rule->message() ? (array) $rule->message() : [get_class($rule)];

            foreach ($messages as $message) {
                $this->messages->add($attribute, $this->makeReplacements(
                    $message, $attribute, get_class($rule), []
                ));
            }
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Validation;

interface CustomValidatorMaker
{
    public function make(array $customerData): CustomValidator;
}

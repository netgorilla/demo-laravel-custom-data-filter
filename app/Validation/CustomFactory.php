<?php

declare(strict_types=1);

namespace App\Validation;

use Illuminate\Validation\Factory;

final class CustomFactory extends Factory
{
    protected function resolve(array $data, array $rules, array $messages, array $customAttributes)
    {
        if (is_null($this->resolver)) {
            return new DefaultCustomValidator($this->translator, $data, $rules, $messages, $customAttributes);
        }

        return call_user_func($this->resolver, $this->translator, $data, $rules, $messages, $customAttributes);
    }
}

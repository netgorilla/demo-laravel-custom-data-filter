<?php

declare(strict_types=1);

namespace App\Validation\TelephoneNumberValidator;

final class DefaultTelephoneNumberValidator implements TelephoneNumberValidator
{
    public function isValid(?string $number, ?string $countryCode = 'GB'): bool
    {
        // This would be swapped out to a regex pattern
        if (strlen($number) === 11) {
            return true;
        }
        return false;
    }
}

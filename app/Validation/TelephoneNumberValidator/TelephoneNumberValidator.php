<?php

declare(strict_types=1);

namespace App\Validation\TelephoneNumberValidator;

interface TelephoneNumberValidator
{
    public function isValid(?string $number, ?string $countryCode = 'GB'): bool;
}

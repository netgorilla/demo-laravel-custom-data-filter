<?php
// @codingStandardsIgnoreFile

declare(strict_types=1);

namespace App\Validation\TelephoneNumberValidator;

use Tests\TestCase;

final class DefaultTelephoneNumberValidatorTest extends TestCase
{
    /** @test */
    public function it_validates_a_phone_number_is_11_chars_long(): void
    {
        $validator = new DefaultTelephoneNumberValidator();

        $validPhoneNumber = '01234567890';
        $shortPhoneNumber = '65487';
        $longPhoneNumber = '654564654654587';

        $this->assertTrue($validator->isValid($validPhoneNumber));
        $this->assertFalse($validator->isValid($shortPhoneNumber));
        $this->assertFalse($validator->isValid($longPhoneNumber));
    }
}

<?php

declare(strict_types=1);

namespace App\Providers;

use App\DataParser\DataParser;
use App\DataParser\DefaultCustomerDataParser;
use App\Validation\CustomFactory;
use App\Validation\DefaultCustomValidatorMaker;
use App\Validation\TelephoneNumberValidator\DefaultTelephoneNumberValidator;
use App\Rules\Address;
use App\Rules\FamilyNameOfPerson;
use App\Rules\TelephoneNumber;
use App\Rules\TitleOfPerson;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $validationFactory = $this->app->make(CustomFactory::class);

        $this->app->bind(
            DataParser::class,
            function () use ($validationFactory) {
                return new DefaultCustomerDataParser(
                    new DefaultCustomValidatorMaker(
                        $this->defaultImportDataValidationRules(),
                        $this->defaultImportDataValidationMessages(),
                        $validationFactory
                    )
                );
            }
        );

    }

    private function defaultImportDataValidationMessages(): array
    {
        return [
            // custom messages for default Laravel validation,
        ];
    }

    private function defaultImportDataValidationRules(): array
    {
        return [
            'name.title' => [
                new TitleOfPerson(),
            ],
            'name.familyName' => [
                new FamilyNameOfPerson(),
            ],
            'address' => [
                new Address(),
            ],
            'phoneNumber' => [
                new TelephoneNumber(
                    // or preferably a library such as giggsey/libphonenumber
                    new DefaultTelephoneNumberValidator()
                ),
            ],
        ];
    }
}

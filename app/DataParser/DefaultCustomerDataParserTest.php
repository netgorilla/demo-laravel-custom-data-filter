<?php
// @codingStandardsIgnoreFile

declare(strict_types=1);

namespace App\DataParser;

use App\Validation\CustomValidatorMaker;
use App\Validation\CustomValidator;
use Illuminate\Support\Collection;
use Prophecy\Argument;
use Tests\TestCase;

final class DefaultCustomerDataParserTest extends TestCase
{
    /** @test */
    public function it_merges_the_validation_array_into_the_record_data(): void
    {
        $mockCustomValidator = $this->prophesize(CustomValidator::class);
        $mockCustomValidator->getTotalDirtinessScore()->willReturn(5);
        $mockCustomValidator->getMessageBag()->willReturn(new Collection([
            'This is a validation message',
        ]));
        $mockCustomValidator->validate()->willReturn();
        $mockCustomValidator->reveal();

        $mockValidatorMaker = $this->prophesize(CustomValidatorMaker::class);
        $mockValidatorMaker->make(Argument::cetera())->willReturn($mockCustomValidator);

        $dataParser = new DefaultCustomerDataParser($mockValidatorMaker->reveal());

        $inputData = new Collection([
            [
                'foo' => 'bar',
            ],
        ]);

        $expectedParsedData = [
            [
                'foo' => 'bar',
                'validation' => [
                    'dirtinessScore' => 5,
                    'messages' => [
                        'This is a validation message',
                    ],
                ],
            ],
        ];

        $this->assertEquals($expectedParsedData, $dataParser->parse($inputData)->all());
    }
}

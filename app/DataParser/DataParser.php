<?php

declare(strict_types=1);

namespace App\DataParser;

use Illuminate\Support\Collection;

interface DataParser
{
    public function parse(Collection $records): Collection;
}

<?php

declare(strict_types=1);

namespace App\DataParser;

use App\Validation\CustomValidatorMaker;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

final class DefaultCustomerDataParser implements DataParser
{
    private CustomValidatorMaker $validatorMaker;

    public function __construct(CustomValidatorMaker $validatorMaker)
    {
        $this->validatorMaker = $validatorMaker;
    }

    public function parse(Collection $records): Collection
    {
        return $records->map(
            function ($record) {
                $validator = $this->validatorMaker->make($record);

                try {
                    $validator->validate();
                } catch(ValidationException $e) {
                    // do nothing for now
                }

                $record['validation'] = [
                    'dirtinessScore' => $validator->getTotalDirtinessScore(),
                    'messages' => $validator->getMessageBag()->all(),
                ];

                return $record;
            }
        );
    }
}

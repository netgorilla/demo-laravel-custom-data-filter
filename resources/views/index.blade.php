<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        The following data has been imported:
        @json($data)
        <hr>
        The following data has been parsed:
        @foreach($parsedData as $record)
            <div style="width: 700px;">
                <dl>
                    @if ($record['name'] ?? false)
                        <dt>Name:</dt>
                        <dd>{{ ($record['name']['title'] . ' ' . $record['name']['givenName'] . ' ' . $record['name']['familyName']) ?? '' }}</dd>
                    @endif
                    @if ($record['address'] ?? false)
                        <dt>Address:</dt>
                        <dd>{{ implode(', ', array_filter(($record['address']))) }}</dd>
                    @endif
                    @if ($record['phoneNumber'] ?? false)
                        <dt>Telephone:</dt>
                        <dd>
                            {{ $record['phoneNumber']['label'] ?? '' }}
                            {{ $record['phoneNumber']['countryCode'] ?? '' }}
                            {{ $record['phoneNumber']['number'] ?? '' }}
                        </dd>
                    @endif
                    @if ($record['validation'] ?? false)
                        <dt>Validation Dirtiness Score:</dt>
                        <dd>{{ $record['validation']['dirtinessScore'] }}</dd>
                        <dt>Validation Messages:</dt>
                        <dd><ul>
                            @foreach($record['validation']['messages'] as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul></dd>
                    @endif
                </dl>

            </div>
        @endforeach
    </body>
</html>
